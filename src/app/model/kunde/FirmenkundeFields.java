package app.model.kunde;

public enum FirmenkundeFields {
	COMPANY(10);

	private int strlen;

	private FirmenkundeFields(int strlen) {
		this.strlen = strlen;
	}

	public int getStrlen() {
		return strlen;
	}

	public FirmenkundeFields getOrdinal(int ordinal) {
		for (FirmenkundeFields pf : FirmenkundeFields.values()) {
			if (pf.ordinal() == ordinal)
				return pf;
		}
		return null;
	}
}