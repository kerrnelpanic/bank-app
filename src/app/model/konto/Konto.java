package app.model.konto;

import java.util.ArrayList;
import java.util.Random;

import app.model.exceptions.TransactionException;

/**
 * some useful information about this very, very complex class
 *
 */
public class Konto {
	private String iban;
	private ArrayList<Transaktion> transactions;
	private Long balance=0L;

	/**
	 * initializes new Konto, adds automatically generated iban
	 */
	public Konto() {
		this.iban = ibanGenerator();
		this.transactions = new ArrayList<Transaktion>();
	}

	/**
	 * @return
	 */
	public String getIban() {
		return iban;
	}

	public ArrayList<Transaktion> getTransactions() {
		return new ArrayList<Transaktion>(transactions);
	}

	/**
	 * @return
	 */
	public Long getBalance() {
		return balance;
	}

	/**
	 * @param ta
	 * @throws TransactionException
	 */
	public void commitTransaction(Transaktion ta) throws TransactionException {
		// pruefen/vorbereiten
		Long amount = ta.getAmount();
		if(amount < 0)
			throw new TransactionException("amount must be greater 0: " + amount);
		
		if (ta.getTransaktionsart() == Transaktionsart.AUSZAHLUNG) {
			if (amount > this.balance)
				throw new TransactionException("Account balance (" + this.balance + ") too low to withdraw " + amount);
			amount *= -1;
		}
		// dokumentieren
		transactions.add(ta);
		// zuweisen
		this.balance += amount;
	}

	@Override
	public String toString() {
		return "\n\t+++++++++++++++++++++++++++++++++++\n\tNo.:\t" + iban + "\n\tbal:\t" + balance
				+ "\n\t+++++++++++++++++++++++++++++++++++\n";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Konto other = (Konto) obj;
		if (iban == null) {
			if (other.iban != null)
				return false;
		} else if (!iban.equals(other.iban))
			return false;
		return true;
	}

	/**
	 * @return a random, German style IBAN
	 */
	private String ibanGenerator() {
		Random r = new Random();
		StringBuilder sb = new StringBuilder();
		sb.append("DE");
		for (int i = 0; i < 20; i++) {
			sb.append(r.nextInt(10));
		}
		return sb.toString();
	}

}