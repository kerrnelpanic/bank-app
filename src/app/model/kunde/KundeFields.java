package app.model.kunde;

public enum KundeFields {
	TELEPHONE(10), EMAIL(30);

	private int strlen;

	private KundeFields(int strlen) {
		this.strlen = strlen;
	}

	public int getStrlen() {
		return strlen;
	}

	public KundeFields getOrdinal(int ordinal) {
		for (KundeFields pf : KundeFields.values()) {
			if (pf.ordinal() == ordinal)
				return pf;
		}
		return null;
	}
}