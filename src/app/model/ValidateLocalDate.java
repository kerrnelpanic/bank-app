package app.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

import app.model.exceptions.InvalidInputException;

/**
 * some useful information about this very, very complex class
 *
 */
public class ValidateLocalDate {
	/**
	 * @param args 3 Strings, ordered as year month day
	 * @return
	 * @throws InvalidInputException
	 */
	public static LocalDate validate(String... args) throws InvalidInputException {
		LocalDate dob;
		final DateTimeFormatter dtfM = DateTimeFormatter.ofPattern("u M d").withResolverStyle(ResolverStyle.STRICT);
		final DateTimeFormatter dtfLLL = DateTimeFormatter.ofPattern("u LLL d").withResolverStyle(ResolverStyle.STRICT);
		final DateTimeFormatter dtfLLLL = DateTimeFormatter.ofPattern("u LLLL d")
				.withResolverStyle(ResolverStyle.STRICT);
		try {
			dob = LocalDate.parse(args[0] + " " + args[1] + " " + args[2], dtfM);
		} catch (DateTimeParseException dtpeM) {
			try {
				dob = LocalDate.parse(args[0] + " " + args[1] + " " + args[2], dtfLLL);
			} catch (Exception dtpeLLL) {
				try {
					dob = LocalDate.parse(args[0] + " " + args[1] + " " + args[2], dtfLLLL);
				} catch (Exception dtpeLLLL) {
					throw new InvalidInputException(
							"input cannot be parsed to a valid Date\n" + dtpeLLLL.getMessage());
				}
			}
		}
		return dob;
	}
}
