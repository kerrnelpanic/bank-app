package app.model.konto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Eine Transaktion enthaelt:<br>
 * � einen Zeitstempel (Datum mit Uhrzeit)<br>
 * � die Transaktionsart (Einzahlung oder Auszahlung)<br>
 * � einen Beschreibungstext und<br>
 * � einen Betrag.<br>
 */
public class Transaktion implements Comparable<Transaktion> {
	private final LocalDateTime timestamp;// TODO leaves out seconds field when seconds=0
	private final Transaktionsart type;
	private final String description;
	private final Long amount;

	/**
	 * @param timestamp
	 * @param transaktionsart
	 * @param description
	 * @param amount
	 */
	public Transaktion(Transaktionsart transaktionsart, String description, long amount) {
		this.timestamp = LocalDateTime.now();
		this.type = transaktionsart;
		this.description = description;
		this.amount = amount;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public Transaktionsart getTransaktionsart() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public Long getAmount() {
		return Long.valueOf(amount);
	}

	@Override
	public String toString() {
		return timestamp.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME) + ", "
				+ type.toString() + ", " + description + ", " + amount.toString();
	}

	@Override
	public int compareTo(Transaktion other) {
		return this.timestamp.compareTo(other.timestamp);
	}
}
