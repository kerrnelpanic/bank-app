package app.model;

public class Ansprechpartner {
	private String firstName;
	private String lastName;
	private String telefone;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		return "\n\t===================================\n\tfirstName\t" + firstName
				+ "\n\tlastName\t" + lastName + "\n\ttelefone\t" + telefone
				+ "\n\t===================================\n";
	}

}