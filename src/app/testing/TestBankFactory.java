package app.testing;

import java.util.Random;

import app.model.Ansprechpartner;
import app.model.Bank;
import app.model.exceptions.InvalidInputException;
import app.model.exceptions.TransactionException;
import app.model.konto.Konto;
import app.model.konto.Transaktion;
import app.model.konto.Transaktionsart;
import app.model.kunde.Adresse;
import app.model.kunde.Firmenkunde;
import app.model.kunde.Kunde;
import app.model.kunde.Privatkunde;
import app.model.randomSource.RandomNameStringGenerator;
import app.view.MainView;

public class TestBankFactory {
	private static StringBuilder progressText = new StringBuilder();
	private static StringBuilder progressBar = new StringBuilder();
	private static int sleepTimerBaseValue = 50;

	public static Bank testDaBank() {
		progressAnimator("Creating bank", 5);

		Random rand = new Random();

		Adresse bankadr = new Adresse();
		try {
			bankadr.setAddressLine1(RandomNameStringGenerator.randomNameString());

			StringBuilder zip = new StringBuilder(Integer.toString(rand.nextInt(99_999)));
			while (zip.length() < 5)
				zip.insert(0, '0');
			bankadr.setZip(zip.toString());

			bankadr.setCity(RandomNameStringGenerator.randomNameString());
		} catch (InvalidInputException e) {
			e.printStackTrace();
		}

		Bank bank = new Bank("Bad Bank",
				RandomNameStringGenerator.randomNameString(7, 7).toUpperCase() + rand.nextInt(99_999), bankadr);

		for (int i = 0; i < 2; i++) {

			progressAnimator("Creating Privatkunde " + (i + 1), 5);
			Kunde k = randomPrivatkunde();

			progressAnimator("Creating Konto for " + k.getCustomerID(), 3);
			k.kontoAdd(new Konto());
			for (int j = 0; j < 2; ++j) {
				try {
					progressAnimator("Generating transaction " + (j + 1), 1);
					k.getAccounts().get(0).commitTransaction(taGenerator());
					bank.kundeAdd(k);
				} catch (TransactionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		for (int i = 0; i < 2; i++) {

			progressAnimator("Creating Firmenkunde " + (i + 1), 5);
			Kunde k = randomFirmenkunde();

			for (int j = 0; j < 2; ++j) {
				progressAnimator("Creating Konto " + (j + 1), 3);
				k.kontoAdd(new Konto());

				for (int ii = 0; ii < 4; ++ii) {
					progressAnimator("Generating transaction " + (ii + 1), 1);
					System.out.println();
//						Thread.sleep(sleepTimer);
					try {
						k.getAccounts().get(0).commitTransaction(taGenerator());
					} catch (TransactionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				bank.kundeAdd(k);

			}
		}
		return bank;
	}

	static Privatkunde randomPrivatkunde() {
		Privatkunde pk = new Privatkunde();
		Random r = new Random();
		try {
			StringBuilder zip = new StringBuilder(Integer.toString(r.nextInt(99_999)));
			while (zip.length() < 5)
				zip.insert(0, '0');

			pk.setAddress(RandomNameStringGenerator.randomNameString() + "str. " + r.nextInt(200),
					RandomNameStringGenerator.randomNameString(), "" + zip,
					RandomNameStringGenerator.randomNameString());
			pk.setDob("" + (r.nextInt(100) + 1900), "" + (r.nextInt(11) + 1), "" + (r.nextInt(30) + 1));
			pk.setEmail(RandomNameStringGenerator.randomNameString().toLowerCase() + "@"
					+ RandomNameStringGenerator.randomNameString().toLowerCase() + "."
					+ RandomNameStringGenerator.randomNameString(2, 3).toLowerCase());
			pk.setFirstName(RandomNameStringGenerator.randomNameString());
			pk.setLastName(RandomNameStringGenerator.randomNameString());
			pk.setTelephone("0" + r.nextInt(9_999_999));
		} catch (InvalidInputException e) {
			System.out.println(e);
		}
		return pk;
	}

	static Firmenkunde randomFirmenkunde() {
		Firmenkunde fk = new Firmenkunde();
		Random r = new Random();
		try {
			StringBuilder zip = new StringBuilder(Integer.toString(r.nextInt(99_999)));
			while (zip.length() < 5)
				zip.insert(0, '0');

			fk.setAddress(RandomNameStringGenerator.randomNameString() + "str. " + r.nextInt(200),
					RandomNameStringGenerator.randomNameString(), "" + zip,
					RandomNameStringGenerator.randomNameString());
			fk.setTelephone("0" + r.nextInt(9_999_999));
			fk.setEmail(RandomNameStringGenerator.randomNameString().toLowerCase() + "@"
					+ RandomNameStringGenerator.randomNameString().toLowerCase() + "."
					+ RandomNameStringGenerator.randomNameString(2, 3).toLowerCase());

			fk.setCompany(RandomNameStringGenerator.randomNameString() + " Inc.");
			fk.setContact(new Ansprechpartner());
		} catch (InvalidInputException e) {
			System.out.println(e);
		}
		fk.setContact(contactGenerator());
		return fk;
	}

	public static Transaktion taGenerator() {
//		LocalDateTime t;
		Transaktionsart tArt;
		StringBuilder desc = new StringBuilder();
		Long amount;
		Random r = new Random();
		// deprecated, timestamp is now created by constructor
//		t = LocalDateTime.now().minus(r.nextInt(1_000_000), ChronoUnit.SECONDS);
		tArt = Transaktionsart.EINZAHLUNG;
		for (int i = 0; i < r.nextInt(4) + 1; i++) {
			desc.append(" " + RandomNameStringGenerator.randomNameString(3, 15));
		}
		desc.deleteCharAt(0);
		amount = (long) r.nextInt(1_000);
		Transaktion ta = new Transaktion(tArt, desc.toString(), amount);

		return ta;
	}

	public static Ansprechpartner contactGenerator() {
		Ansprechpartner contact = new Ansprechpartner();
		contact.setFirstName("Beagle");
		contact.setLastName("Boy");
		contact.setTelefone("176-176");

		return contact;
	}

	static void progressAnimator(String concat, int sleepTimeFactor) {
		progressText.append(concat+System.lineSeparator());
		progressBar.append(".");
		MainView.clearScreen();
		System.out.println(progressText);
		System.out.println(progressBar);
		try {
			Thread.sleep(sleepTimerBaseValue * sleepTimeFactor);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
