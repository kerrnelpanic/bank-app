package app.model.kunde;

import java.time.LocalDate;

import app.model.ValidateLocalDate;
import app.model.exceptions.InvalidInputException;

/**
 * some useful information about this very, very complex class
 *
 */
public class Privatkunde extends Kunde {

	private String firstName;
	private String lastName;
	private LocalDate dob;

	/**
	 * no-args constructor
	 */
	public Privatkunde() {

	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName String of max length =
	 *                  PRIVATKUNDE_FIELDS.FIRSTNAME.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setFirstName(String firstName) throws InvalidInputException {
		if (firstName.length() > PrivatkundeFields.FIRSTNAME.getStrlen())
			throw new InvalidInputException("input \"" + firstName + "\" to long for " + PrivatkundeFields.FIRSTNAME
					+ " allowed length: " + PrivatkundeFields.FIRSTNAME.getStrlen());
		if (firstName.length() == 0)
			throw new InvalidInputException(
					"input \"" + firstName + "\" cannot be enpty for " + PrivatkundeFields.FIRSTNAME.getStrlen());

		this.firstName = firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName String of max length =
	 *                 PRIVATKUNDE_FIELDS.LASTNAME.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setLastName(String lastName) throws InvalidInputException {
		if (lastName.length() > PrivatkundeFields.LASTNAME.getStrlen())
			throw new InvalidInputException("input \"" + lastName + "\" to long for " + PrivatkundeFields.LASTNAME
					+ " allowed length: " + PrivatkundeFields.LASTNAME.getStrlen());
		if (lastName.length() == 0)
			throw new InvalidInputException(
					"input \"" + lastName + "\" cannot be enpty for " + PrivatkundeFields.LASTNAME);

		this.lastName = lastName;
	}

	/**
	 * @return
	 */
	public LocalDate getDob() {
		return dob;
	}

	/**
	 * @param args as defined by ValidateLocalDate.validate()
	 * @throws InvalidInputException
	 */
	public void setDob(String... args) throws InvalidInputException {
		this.dob = ValidateLocalDate.validate(args);
	}

	/**
	 * @param dob
	 */
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "\n___________________________________________\nPrivatkunde\nName:\t" + lastName + ", " + firstName
				+ "\nGeb.:\t" + dob + super.toString() + "\n___________________________________________\n";
	}

	@Override
	public String getName() {
		return this.firstName + " " + this.lastName;
	}

//	@Override
//	public int compareTo(Kunde o) {
//		// TODO Auto-generated method stub
//		return 0;
//	}

}
