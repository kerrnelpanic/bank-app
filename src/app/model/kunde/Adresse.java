package app.model.kunde;

import app.model.exceptions.InvalidInputException;

/**
 * some useful information about this very, very complex class
 *
 */
/**
 * @author CC-Student
 *
 */
/**
 * @author CC-Student
 *
 */
public class Adresse {
	private String addressLine1;
	private String addressLine2 = "";
	private String zip;
	private String city;

	
	/**
	 * no-args constructor
	 */
	public Adresse() {
	}

	/**
	 * copy constructor
	 * @param blueprint
	 */
	public Adresse(Adresse blueprint) {
		this.addressLine1 = blueprint.addressLine1;
		this.addressLine2 = blueprint.addressLine2;
		this.zip = blueprint.zip;
		this.city = blueprint.city;
	}

	/**
	 * @return
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 String of max length =
	 *                     ADDRESS_FIELDS.ADDRESSLINE1.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setAddressLine1(String addressLine1) throws InvalidInputException {
		if (addressLine1.length() > AddressFields.ADDRESSLINE1.getStrlen())
			throw new InvalidInputException("input \"" + addressLine1 + "\" to long for " + AddressFields.ADDRESSLINE1
					+ " allowed length: " + AddressFields.ADDRESSLINE1.getStrlen());
		if (addressLine1.length() == 0)
			throw new InvalidInputException(
					"input \"" + addressLine1 + "\" cannot be enpty for " + AddressFields.ADDRESSLINE1);

		this.addressLine1 = addressLine1;
	}

	/**
	 * @return
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 String of max length =
	 *                     ADDRESS_FIELDS.ADDRESSLINE2.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setAddressLine2(String addressLine2) throws InvalidInputException {
		if (addressLine2.length() > AddressFields.ADDRESSLINE2.getStrlen())
			throw new InvalidInputException("input \"" + addressLine2 + "\" to long for " + AddressFields.ADDRESSLINE2
					+ " allowed length: " + AddressFields.ADDRESSLINE2.getStrlen());

		this.addressLine2 = addressLine2;
	}

	/**
	 * @return
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip String representing numerals of length =
	 *            ADDRESS_FIELDS.ZIP.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setZip(String zip) throws InvalidInputException {
		if (zip.length() != AddressFields.ZIP.getStrlen())
			throw new InvalidInputException("input \"" + zip + "\" not appropriate for " + AddressFields.ZIP
					+ " length, must be " + AddressFields.ZIP.getStrlen() + " numbers");
		try {
			int inputZip = Integer.parseInt(zip);
			this.zip = "D-" + inputZip;
		} catch (NumberFormatException nfe) {
			throw new InvalidInputException("not a valid zip code " + zip);
		}
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city String of max length = ADDRESS_FIELDS.CITY.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setCity(String city) throws InvalidInputException {
		if (city.length() > AddressFields.CITY.getStrlen())
			throw new InvalidInputException("input \"" + city + "\" to long for " + AddressFields.CITY
					+ " allowed length: " + AddressFields.CITY.getStrlen());

		this.city = city;
	}
	
	@Override
	public String toString() {
		return "\n\t" + addressLine1 + "\n\t" + addressLine2 + "\n\t" + zip + " " + city;
	}
}