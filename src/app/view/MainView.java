package app.view;

import static java.lang.System.out;

import java.util.Comparator;
import java.util.LinkedList;

import app.controller.MainController;
import app.model.TransactionFile;
import app.model.konto.Konto;
import app.model.kunde.Firmenkunde;
import app.model.kunde.Kunde;
import app.model.kunde.Privatkunde;

/**
 * supplies Views for MainController
 *
 */
public class MainView {
	/**
	 * prompts the main menu in the console
	 */
	public static void showMainMenu() {
		out.println("(01) Privatkunde anlegen");
		out.println("(02) Firmenkunde anlegen");
		out.println("(03) Konto anlegen und Kundennummer zuordnen");
		out.println("(04) Kunde mit Konten anzeigen (Auswahl durch Kundennummer)");
		out.println("(05) Kunde mit Konten anzeigen (Auswahl durch Name)");
		out.println("(06) Konto anzeigen (Auswahl durch IBAN)");
		out.println("(07) Alle Kunden unsortiert anzeigen");
		out.println("(08) Alle Kunden sortiert nach aufsteigender Kundennummer anzeigen");
		out.println("(09) Alle Konten unsortiert anzeigen");
		out.println("(10) Geld auf Konto einzahlen");
		out.println("(11) Geld vom Konto abheben");
		out.println("(12) Transaktionsliste absteigend sortiert nach Zeitstempel anzeigen");
		out.println("(13) Transaktionsliste aufsteigend sortiert nach Zeitstempel speichern");
		out.println("(14) Transaktionsliste einlesen");
		out.println("(15) GUI für Ein- und Auszahlungen öffnen");
		out.println("(16) Beenden");
	}

	/**
	 * prompts information about customer <b>k</b>
	 * 
	 * @param k
	 */
	public static void showCustomerDetails(Kunde k) {
		clearScreen();
		System.out.println(k);
	}

	/**
	 * prompts a list to select a single Customer by Kunde.customerId
	 * 
	 * @param kList
	 */
	public static void showCustomerSelectionById(LinkedList<Kunde> kList) {
		kList.forEach((kunde) -> {
			System.out.println((kList.indexOf(kunde) + 1) + ": " + kunde.getCustomerID());
		});
	}

	/**
	 * prompts a list to select a single Customer by Firmenkunde.company or
	 * Privatkunde.lastname and Privatkunde.firstname
	 * 
	 * @param kList
	 */
	public static void showCustomerSelectionByName(LinkedList<Kunde> kList) {
		kList.forEach((kunde) -> {
			System.out.print((kList.indexOf(kunde) + 1) + ": ");
			showCustomerName(kunde);
		});
	}

	/**
	 * prompts identification attribute of <b>k</b>
	 * 
	 * @param k
	 */
	public static void showCustomerName(Kunde k) {
		if (k instanceof Firmenkunde)
			System.out.println(((Firmenkunde) k).getCompany());
		else // TODO make it save against additional Kunde subtypes
			System.out.println(((Privatkunde) k).getLastName() + ", " + ((Privatkunde) k).getFirstName());
	}

	/**
	 * prompts list of all customers of <b>Bank.B</b>
	 * 
	 * @param c function to sort customers, if null no sorting is applied
	 */
	public static void showAllCustomers(Comparator<Kunde> c) {
		LinkedList<Kunde> kl = MainController.B.getCustomers();
		if (c != null) {
			kl.sort(c);
		}
		clearScreen();
		out.print(kl);
	}

	/**
	 * prints details of account <b>{@code}</b>
	 * 
	 * @param k
	 */
	public static void showAccountDetail(Konto k) {
		clearScreen();
		out.print(k);
	}

	/**
	 * shows all Accounts in <b>Bank.B</b>
	 */
	public static void showAccounts() {
		clearScreen();
		MainController.B.getCustomers().forEach((kunde) -> {
			out.print(kunde.getAccounts());
		});
	}

	/**
	 * shows a list to select a single account
	 * 
	 * @param accounts
	 */
	public static void showAccountSelectionByIBAN(LinkedList<Konto> accounts) {
		accounts.forEach((acount) -> {
			System.out.println((accounts.indexOf(acount) + 1) + ": " + acount.getIban());
		});
	}

	/**
	 * prints transactions on console ordered by date, descending
	 */
	public static void showTransactions(LinkedList<String[]> transactions) {
		clearScreen();

		String[] csvHeader = TransactionFile.getTransactionHeader();

		// padding section
		int[] template = padLengths(transactions);
		for (int i = 0; i < template.length; i++) {
			if (template[i] < csvHeader[i].length())
				template[i] = csvHeader[i].length();
		}

		// header section
		for (int i = 0; i < csvHeader.length; ++i) {
			StringBuilder sb = new StringBuilder(csvHeader[i]);
			while (sb.length() < template[i])
				sb.append(" ");
			out.print(sb + "\t");
		}
		out.println();

		// transactions section
		transactions.sort((transaction1, transaction2) -> {
			return transaction1[1].compareTo(transaction2[1]) * -1;
		});
		transactions.forEach((transaction) -> {
			for (int i = 0; i < transaction.length; ++i) {
				StringBuilder sb = new StringBuilder(transaction[i].trim());
				while (sb.length() < template[i]) {
					sb.append(" ");
				}
				out.print(sb + "\t");
			}
			out.println();
		});

	}

	private static int[] padLengths(LinkedList<String[]> transactions) {
		int[] maxLengths = new int[transactions.get(0).length];
		for (int i = 0; i < maxLengths.length; ++i) {
			for (String[] str : transactions) {
				for (int j = 0; j < maxLengths.length; ++j) {
					if (maxLengths[j] < str[j].length())
						maxLengths[j] = str[j].length();
				}
			}
		}
		return maxLengths;
	}

	/**
	 * moves the recent content of the console out of app.view
	 */
	public static void clearScreen() {
		for (int i = 0; i < 10; i++) {
			out.print("\n\n\n\n\n\n\n\n\n\n");
		}
	}

	/**
	 * prompts <b>message</b> to indicate wrong user input
	 * 
	 * @param message
	 */
	public static void retryPrompt(String message) {
		clearScreen();
		System.out.println(message);
		System.out.println("Bitte mach's ordentlich");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}