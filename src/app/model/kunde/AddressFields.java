package app.model.kunde;

public enum AddressFields {
	ADDRESSLINE1(15), ADDRESSLINE2(10), ZIP(5), CITY(10);

	private int strlen;

	private AddressFields(int strlen) {
		this.strlen = strlen;
	}

	public int getStrlen() {
		return strlen;
	}

	public AddressFields getOrdinal(int ordinal) {
		for (AddressFields pf : AddressFields.values()) {
			if (pf.ordinal() == ordinal)
				return pf;
		}
		return null;
	}
}