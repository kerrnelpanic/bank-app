package app.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import app.model.exceptions.TransactionException;
import app.model.konto.Konto;
import app.model.konto.Transaktion;
import app.model.konto.Transaktionsart;
import app.model.kunde.Kunde;
import app.view.GuiView;

public class GuiController implements ActionListener, WindowListener, KeyListener {

	GuiView view;
	Kunde selectedCustomer;

	public GuiController(GuiView view) {
		this.view = view;
		this.selectedCustomer = MainController.B.getCustomers().get(0);
	}

	private void redrawFrame() {
		view.setTitle("Kunde: " + selectedCustomer.getName());
		registerCustomersInMenu();
		view.txaCustomers.setText(selectedCustomer.toString());
		if (selectedCustomer.getAccounts().size() < 1) {
			view.txfAmount.setEnabled(false);
			view.txfDescription.setEnabled(false);
		} else {
			view.txfAmount.setEnabled(true);
			view.txfDescription.setEnabled(true);
		}

		view.lblBalanceValue.setText("");

		view.cboxAccounts.removeActionListener(this);
		view.cboxAccounts.removeAllItems();
		view.cboxAccounts.addActionListener(this);
		registerAccountsInCbox();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				view.txfDescription.requestFocusInWindow();
				view.spCustomers.getVerticalScrollBar().setValue(0);
			}
		});
	}

	private void registerCustomersInMenu() {
		view.submCustomers.removeAll();
		MainController.B.getCustomers().forEach(k -> {
			JMenuItem jmi = new JMenuItem(k.getName());
			view.submCustomers.add(jmi);
			jmi.addActionListener(this);
		});
	}

	private void registerAccountsInCbox() {
		for (Konto k : selectedCustomer.getAccounts()) {
			view.cboxAccounts.addItem(k.getIban());
		}
	}

	private void refreshAccountInfo() {
		view.tmodTransactions.setRowCount(0);
		view.txaCustomers.setText(selectedCustomer.toString());
		selectedCustomer.getAccounts().forEach(acc -> {
			if (acc.getIban().equals(view.cboxAccounts.getSelectedItem().toString())) {
				acc.getTransactions().forEach(ta -> {
					String[] taStrings = ta.toString().split(",");
					view.tmodTransactions.insertRow(view.tmodTransactions.getRowCount(), taStrings);
				});
				view.lblBalanceValue.setText(acc.getBalance().toString());
			}
		});
	}

	private Konto getAccount(String iban) {
		Konto konto = null;
		outer: for (Kunde k : MainController.B.getCustomers())
			for (Konto kto : k.getAccounts())
				if (kto.getIban().equals(iban)) {
					konto = kto;
					break outer;
				}
		if (konto != null)
			view.lblBalanceValue.setText(konto.getBalance().toString());
		return konto;
	}

	private void switchTransactionType() {
		if (view.btnTransactionType.getText() == "Einzahlung")
			view.btnTransactionType.setText("Auszahlung");
		else {
			view.btnTransactionType.setText("Einzahlung");
		}
	}

	private void commitTransaction() throws NumberFormatException, TransactionException {
		Konto k = getAccount(view.cboxAccounts.getSelectedItem().toString());
		Transaktionsart ta;
		if (view.btnTransactionType.getText().toLowerCase().equals(Transaktionsart.EINZAHLUNG.toString().toLowerCase()))
			ta = Transaktionsart.EINZAHLUNG;
		else
			ta = Transaktionsart.AUSZAHLUNG;

		k.commitTransaction(
				new Transaktion(ta, view.txfDescription.getText(), Long.parseLong(view.txfAmount.getText())));

		refreshAccountInfo();
		view.txfAmount.setText("");
		view.txfDescription.setText("");
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == view.miInfo) {
			JOptionPane.showMessageDialog(view, view.txaInfo, "Informationen", JOptionPane.INFORMATION_MESSAGE);
		}

		if (e.getSource() == view.btnTransactionType) {
			switchTransactionType();
			view.txfDescription.requestFocusInWindow();
		}

		if (e.getSource() == view.cboxAccounts) {
			refreshAccountInfo();
			view.txfDescription.requestFocusInWindow();
		}

		// TODO improve this, it's ugly (for every action, this loop is performed, no
		// matter which action)
		for (Kunde k : MainController.B.getCustomers()) {
			if (k.getName().equals(e.getActionCommand())) {
				selectedCustomer = k;
				redrawFrame();
			}
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		if (view.getTitle().length() < 1)
			redrawFrame();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (e.getSource() == view.txfAmount || e.getSource() == view.txfDescription) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER && view.txfAmount.getText().length() > 0
					&& view.txfDescription.getText().length() > 0)
				try {
					commitTransaction();
					refreshAccountInfo();
				} catch (NumberFormatException | TransactionException nf_t_e) {
					JOptionPane.showMessageDialog(view, "Verarbeitung nicht möglich: " + nf_t_e.getMessage(),
							"Es ist ein Problem aufgetreten", JOptionPane.ERROR_MESSAGE);
					view.txfAmount.setText("");
				}
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
