package app.controller;

import static app.view.MainView.clearScreen;
import static app.view.MainView.retryPrompt;
import static app.view.MainView.showAccountDetail;
import static app.view.MainView.showAccountSelectionByIBAN;
import static app.view.MainView.showAccounts;
import static app.view.MainView.showAllCustomers;
import static app.view.MainView.showCustomerDetails;
import static app.view.MainView.showCustomerSelectionById;
import static app.view.MainView.showCustomerSelectionByName;
import static app.view.MainView.showMainMenu;
import static app.view.MainView.showTransactions;

import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;

import app.model.Bank;
import app.model.TransactionFile;
import app.model.exceptions.CustomerAlreadyExistingException;
import app.model.exceptions.NoCustomersException;
import app.model.exceptions.TransactionException;
import app.model.konto.Konto;
import app.model.konto.Transaktion;
import app.model.konto.Transaktionsart;
import app.model.kunde.Firmenkunde;
import app.model.kunde.Kunde;
import app.model.kunde.Privatkunde;
import app.testing.TestBankFactory;
import app.view.GuiView;

/**
 * For the sake of having some data to play with, <b>Bank B</b> is initialized with random data.
 *
 */
public class MainController {

	static Scanner scannerIn = new Scanner(System.in);
	public static final Bank B = TestBankFactory.testDaBank();
	public static LinkedList<String[]> availableTransactions = new LinkedList<>();

	/**
	 * entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int userChoice = 0;

		do {
			clearScreen();
			showMainMenu();
			userChoice = scannerIn.nextInt();
			scannerIn.nextLine();
			userChoice = dispatcher(userChoice);
		} while (true);
	}

	static int dispatcher(int userChoice) {
		switch (userChoice) {
		case 1:
			try {
				CreateCustomerController.newKunde(Privatkunde.class.getName());
			} catch (CustomerAlreadyExistingException caee) {
				System.out.println(caee.getMessage());
				confirm();
			}
			break;
		case 2:
			try {
				CreateCustomerController.newKunde(Firmenkunde.class.getName());
			} catch (CustomerAlreadyExistingException caee) {
				System.out.println(caee.getMessage());
				confirm();
			}
			break;
		case 3:
			addNewAccountToCustomer();
			confirm();
			break;
		case 4:// KN
			selectCustomerById(B.getCustomers());
			confirm();
			break;
		case 5:// name
			selectCustomerByName(B.getCustomers());
			confirm();
			break;
		case 6:
			selectAccount();
			confirm();
			break;
		case 7:// unsorted
			showAllCustomers(null);
			confirm();
			break;
		case 8:// sorted ascending
			showAllCustomers((k1, k2) -> k1.compareTo(k2));
			confirm();
			break;
		case 9:// unsorted
			showAccounts();
			confirm();
			break;
		case 10:
			commitTransaction(Transaktionsart.EINZAHLUNG);
			confirm();
			break;
		case 11:
			commitTransaction(Transaktionsart.AUSZAHLUNG);
			confirm();
			break;
		case 12:// from ram and availableTransactions if they exist
			displayTransactions();
			confirm();
			break;
		case 13:// save transaction file
			saveTransactions();
			confirm();
			break;
		case 14:// read transaction file
			retrieveTransactionsFromDisk();
			confirm();
			break;
		case 15:
			try {
				runGui();
			} catch (NoCustomersException nce) {
				System.out.println(nce.getMessage());
				confirm();
			}
			confirm();
			break;
		case 16:
			System.exit(0);
			break;
		default:
		}

		return userChoice;
	}

	/**
	 * @param userMessage to be prompted at user
	 */
	static String getUserInput(String userMessage) {
		System.out.println(userMessage);
		String userInput =
//				scannerIn.next();
				scannerIn.nextLine();
		return userInput;
	}

	/**
	 * halt until user confirms
	 */
	static void confirm() {
		System.out.println("\nWeiter mit Entertaste\t");
		scannerIn.nextLine();
	}

	/**
	 * retrieves customer information for a single customer
	 */
	static Kunde selectCustomerById(LinkedList<Kunde> kl) {
		clearScreen();

		showCustomerSelectionById(kl);

		int choice = -1;
		while (choice > kl.size() || choice < 1) {
			try {
				choice = Integer.parseInt(getUserInput("welchen Kunden? 1 - " + kl.size()));
			} catch (NumberFormatException nfe) {
				retryPrompt("Cannot find customer: " + nfe.getMessage());
			}
		}
		Kunde k = B.getCustomers().get(choice - 1);
		showCustomerDetails(k);
		return k;
	}

	/**
	 * retrieves customer information for a single customer
	 */
	static Kunde selectCustomerByName(LinkedList<Kunde> kl) {
		clearScreen();

		showCustomerSelectionByName(kl);

		int choice = -1;
		while (choice > kl.size() || choice < 1) {
			try {
				choice = Integer.parseInt(getUserInput("welchen Kunden? 1 - " + kl.size()));
			} catch (NumberFormatException nfe) {
				retryPrompt("Cannot find customer: " + nfe.getMessage());
			}
		}
		Kunde k = B.getCustomers().get(choice - 1);
		showCustomerDetails(k);
		return k;
	}

	/**
	 * retrieves account information for a single account
	 */
	static Konto selectAccount() {
		clearScreen();

		LinkedList<Konto> kl = new LinkedList<Konto>();
		B.getCustomers().forEach((kunde) -> {
			kl.addAll(kunde.getAccounts());
		});

		showAccountSelectionByIBAN(kl);

		int choice = -1;
		while (choice > kl.size() || choice < 1) {
			try {
				choice = Integer.parseInt(getUserInput("welches Konto? 1 - " + kl.size()));
			} catch (NumberFormatException nfe) {
				retryPrompt("Cannot find account: " + nfe.getMessage());
			}
		}
		Konto k = kl.get(choice - 1);
		showAccountDetail(k);
		return k;
	}

	/**
	 * selects a customer from <b>B.customers</b> and adds a new account
	 */
	static void addNewAccountToCustomer() {
		clearScreen();

		Kunde k;
		Character choice = 0;
		while (!(choice.equals((Character) 'n') || choice.equals((Character) 'i'))) {
			choice = getUserInput("n: name\ni:id").charAt(0);
		}
		if (choice == 'n')
			k = selectCustomerByName(B.getCustomers());
		else
			k = selectCustomerById(B.getCustomers());

		k.kontoAdd(new Konto());

		clearScreen();
		showCustomerDetails(k);
	}

	/**
	 * @param tArt
	 * @return
	 */
	private static Transaktion newTransactionFromUserInput(Transaktionsart tArt) {
		Transaktion ta;
		String description;
		Long amount = null;

		description = getUserInput("Beschreibung ");

		while (amount == null || amount < 0) {
			try {
				amount = Long.parseLong(getUserInput("Betrag "));
				if (amount < 0)
					throw new NumberFormatException("Must be greater than 0: " + amount);
			} catch (NumberFormatException nfe) {
				retryPrompt("Please insert valid amount: " + nfe.getMessage());
			}
		}
		ta = new Transaktion(tArt, description, amount);
		return ta;
	}

	/**
	 * performs the transaction <b>tArt</b> to an account and with values selected
	 * by a user
	 * 
	 * @param tArt
	 */
	static void commitTransaction(Transaktionsart tArt) {
		Kunde k = selectCustomerById(B.getCustomers());
		LinkedList<Konto> kl = new LinkedList<>(k.getAccounts());

		int choice = -1;
		while (choice > kl.size() || choice < 1) {
			showAccountSelectionByIBAN(kl);
			try {
				choice = Integer.parseInt(getUserInput("welches Konto? 1 - " + kl.size()));
			} catch (NumberFormatException nfe) {
				retryPrompt("Cannot find account: " + nfe.getMessage());
			}
		}

		ArrayList<Transaktion> al = null;
		while (al == null) {
			System.out.println(kl.get(choice - 1));
			try {
				Transaktion ta = newTransactionFromUserInput(tArt);
				kl.get(choice - 1).commitTransaction(ta);
				al = new ArrayList<>(kl.get(choice - 1).getTransactions());
				System.out.println(al.get(al.size() - 1));

			} catch (TransactionException te) {
				retryPrompt("Cannot perform transaction: " + te.getMessage());

			}
		}

	}

	/**
	 * prompts all available transactions
	 * 
	 * @param transactions
	 * 
	 */
	private static void displayTransactions() {
		// from ram
		LinkedList<String[]> transactionsAsStrings = prepareRecentTransactionsList(B.getCustomers());
		// from file
		transactionsAsStrings.addAll(availableTransactions);
		// prompt
		showTransactions(transactionsAsStrings);
	}

	/**
	 * save recent transactions to disk
	 */
	static boolean saveTransactions() {// TODO dubletten
		LinkedList<String[]> transactions = availableTransactions;

		transactions.addAll(prepareRecentTransactionsList(B.getCustomers()));

		transactions.sort((String[] transaction1, String[] transaction2) -> {
			return transaction1[1].compareTo(transaction2[1]);
		});

		boolean success = TransactionFile.flushTransactions(transactions);
		clearScreen();
		if (success) {
			System.out.println("Successfully saved " + transactions.size() + " transactions.");
		} else
			System.out.println("Unknown error while saving.");
		return success;
	}

	/**
	 * read transactions from file to RAM
	 */
	static void retrieveTransactionsFromDisk() {
		LinkedList<String[]> transactions = new LinkedList<>();

		transactions.addAll(TransactionFile.readTransactions());

		availableTransactions = transactions;

		clearScreen();
		System.out.println(transactions.size() + " transactions read from disk.");
	}

	/**
	 * prepares all recent transactions for displaying or saving to disk
	 * 
	 * @return LinkedList of transactions ordered by <b>timestamp</b> descending
	 */
	static LinkedList<String[]> prepareRecentTransactionsList(LinkedList<Kunde> customers) {
		LinkedList<String[]> transactions = new LinkedList<String[]>();

		customers.forEach((c) -> {
			c.getAccounts().forEach((a) -> {
				if (a.getTransactions() != null) {
					a.getTransactions().forEach(t -> {
						String[] str = new String[5];
						str[0] = a.getIban().toString();
						str[1] = t.getTimestamp().truncatedTo(ChronoUnit.SECONDS)
								.format(DateTimeFormatter.ISO_DATE_TIME);
						str[2] = t.getTransaktionsart().toString();
						str[3] = t.getDescription().toString();
						str[4] = t.getAmount().toString();
						transactions.add(str);
					});
				}
			});
		});
		transactions.sort((transaction1, transaction2) -> {
			return transaction1[1].compareTo(transaction2[1]);
		});

		return transactions;
	}

	/**
	 * starts the gui for new transactions
	 * 
	 * @throws NoCustomersException
	 */
	static void runGui() throws NoCustomersException {
		if (MainController.B.getCustomers().size() < 1)
			throw new NoCustomersException("No customer found");
		new GuiView();
	}
}
