package app.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import app.controller.GuiController;
import app.model.TransactionFile;

public class GuiView extends JFrame {
	private static final long serialVersionUID = 1L;
	Font font = new Font("Courier New", Font.PLAIN, 12);
	Color clrDataModeDB = Color.getHSBColor(.3f, .2f, .8f);
	Color clrDataModeFile = Color.getHSBColor(.55f, .2f, .8f);
	DefaultTableCellRenderer tblCellRenderRight = new DefaultTableCellRenderer();

	private GuiController controller;

	public JMenuBar mb;
	public JMenu menuCustomers;
	public JMenu submCustomers;
	public JMenuItem miInfo;

	public JScrollPane spCustomers;
	public JTextArea txaCustomers;

	public JPanel pnlUI;
	public JButton btnTransactionType;
	public JLabel lblBalance;
	public JLabel lblBalanceValue;
	public JLabel lblAmount;
	public JTextField txfAmount;
	public JLabel lblDescription;
	public JTextField txfDescription;

	public DefaultTableModel tmodTransactions;
	public JTable tblTransactions;
	public JScrollPane spTransactions;

	public JPanel pnlCustomer;
	public JPanel pnlContent;

	public JTextArea txaInfo;

	public JComboBox<String> cboxAccounts;

	public GuiView() {
		this.controller = new GuiController(this);

		initComponents();

		mb.add(menuCustomers);
		menuCustomers.add(submCustomers);
		mb.add(miInfo);
		this.setJMenuBar(mb);

		pnlUI.add(btnTransactionType);
		pnlUI.add(lblBalance);
		pnlUI.add(lblDescription);
		pnlUI.add(lblAmount);
		pnlUI.add(cboxAccounts);
		pnlUI.add(lblBalanceValue);
		pnlUI.add(txfDescription);
		pnlUI.add(txfAmount);

		pnlCustomer.add(spCustomers);
		pnlCustomer.add(pnlUI);
		pnlCustomer.add(spTransactions);

		pnlContent.add(pnlCustomer);

		miInfo.addActionListener(controller);
		cboxAccounts.addActionListener(controller);
		btnTransactionType.addActionListener(controller);
		txfAmount.addKeyListener(controller);
		txfDescription.addKeyListener(controller);

		this.addWindowListener(controller);

		spCustomers.setPreferredSize(new Dimension(790, 300));
		spCustomers.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		spCustomers.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		tblCellRenderRight.setHorizontalAlignment(JLabel.RIGHT);
		spTransactions.setPreferredSize(new Dimension(790, 300));
		spTransactions.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		spTransactions.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		spTransactions.getViewport().setBackground(Color.WHITE);

		pnlUI.setLayout(new GridLayout(2, 4, 20, 5));
		pnlCustomer.setPreferredSize(new Dimension(800, 700));

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setContentPane(pnlContent);
		this.setSize(810, 736);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initComponents() {

		mb = new JMenuBar();
		menuCustomers = new JMenu("Kunden");
		submCustomers = new JMenu("Kunde wählen");
		miInfo = new JMenuItem("Info");

		txaCustomers = new JTextArea();
		txaCustomers.setFont(font);
		txaCustomers.setEditable(false);
		spCustomers = new JScrollPane(txaCustomers);

		cboxAccounts = new JComboBox<String>();
		cboxAccounts.setFont(font);
		cboxAccounts.setBackground(Color.WHITE);
		lblBalance = new JLabel("Kontostand");
		lblBalance.setFont(font);
		lblBalance.setHorizontalAlignment(SwingConstants.CENTER);
		lblBalanceValue = new JLabel();
		lblBalanceValue.setFont(font);
		lblBalanceValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblBalanceValue.setOpaque(true);
		lblAmount = new JLabel("überweisungsbetrag");
		lblAmount.setFont(font);
		txfAmount = new JTextField();
		txfAmount.setFont(font);
		txfAmount.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDescription = new JLabel("Beschreibungstext");
		lblDescription.setFont(font);
		txfDescription = new JTextField();
		txfDescription.setFont(font);
		btnTransactionType = new JButton("Einzahlung");
		btnTransactionType.setFont(font);

		tmodTransactions = new DefaultTableModel();
		for (int i = 1; i < TransactionFile.getTransactionHeader().length; i++) {
			tmodTransactions.addColumn(TransactionFile.getTransactionHeader()[i]);
		}
		tblTransactions = new JTable(tmodTransactions);
		tblTransactions.setFont(font);
		tblTransactions.setEnabled(false);
		tblTransactions.getColumnModel().getColumn(0).setMinWidth(140);
		tblTransactions.getColumnModel().getColumn(0).setMaxWidth(140);
		tblTransactions.getColumnModel().getColumn(1).setMinWidth(100);
		tblTransactions.getColumnModel().getColumn(1).setMaxWidth(100);
//		tblTransactions.getColumnModel().getColumn(2).setMinWidth(200);
//		tblTransactions.getColumnModel().getColumn(2).setMaxWidth(200);
		tblTransactions.getColumnModel().getColumn(3).setMinWidth(150);
		tblTransactions.getColumnModel().getColumn(3).setMaxWidth(150);
		tblTransactions.getColumnModel().getColumn(3).setCellRenderer(tblCellRenderRight);
		spTransactions = new JScrollPane(tblTransactions);

		txaInfo = new JTextArea();
		txaInfo.setEditable(false);
		txaInfo.setFont(font);
		txaInfo.setOpaque(false);
		txaInfo.append("Kontoverwaltung");
		txaInfo.append(System.lineSeparator());
		txaInfo.append("Version: 2.0.2");
		txaInfo.append(System.lineSeparator());
		txaInfo.append("");
		txaInfo.append(System.lineSeparator());
		txaInfo.append("Alt+F4\tBeenden");
		txaInfo.append(System.lineSeparator());
		txaInfo.append("");
		txaInfo.append(System.lineSeparator());
		txaInfo.append("Baldur Kellner");

		pnlUI = new JPanel();
		pnlCustomer = new JPanel();
		pnlContent = new JPanel();

	}

}
