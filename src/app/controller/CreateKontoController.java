package app.controller;

import app.model.konto.Konto;

/**
 * some useful information about this very, very complex class
 *
 */
public class CreateKontoController {
	/**
	 * no-args constructor
	 * @return
	 */
	public static Konto createKonto() {
		return new Konto();
	}
}
