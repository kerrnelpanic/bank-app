package app.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MainMenuTest {

	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
//	private final Menu m = new Menu();
	private final String clearScreen = "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n"
			+ "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n"
			+ "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n" + "\n\n\n\n\n\n\n\n\n\n";

	@BeforeEach
	public void setUp() {
		System.setOut(new PrintStream(outputStreamCaptor));
	}

	@AfterEach
	public void tearDown() {
		System.setOut(standardOut);
	}

	@Test
	void testShowMainMenue() {
		// arrange
		String expected = "(01) Privatkunde anlegen\r\n" + "(02) Firmenkunde anlegen\r\n"
				+ "(03) Konto anlegen und Kundennummer zuordnen\r\n"
				+ "(04) Kunde mit Konten anzeigen (Auswahl durch Kundennummer)\r\n"
				+ "(05) Kunde mit Konten anzeigen (Auswahl durch Name)\r\n"
				+ "(06) Konto anzeigen (Auswahl durch IBAN)\r\n" + "(07) Alle Kunden unsortiert anzeigen\r\n"
				+ "(08) Alle Kunden sortiert nach aufsteigender Kundennummer anzeigen\r\n"
				+ "(09) Alle Konten unsortiert anzeigen\r\n" + "(10) Beenden\r\n";
		// act
//		MainController.showMainMenu();
		// assert
		assertEquals(expected, outputStreamCaptor.toString());
	}

	@Test
	void testClearScreen() {
		String expected = clearScreen;

//		MainController.clearScreen();

		assertEquals(expected, outputStreamCaptor.toString());
	}
	
	@Test
	void testOptionsSelector()
	{
		//hard to test, needs mocking to generate user input
//		int actual = Menu.optionSelector();
		
		
		
//		assertEquals(1, actual);
	}

	@Test
	void testRetry() {
		String expected = clearScreen + "Bitte mach's ordentlich\r\n";

//		MainController.retry();

		assertEquals(expected, outputStreamCaptor.toString());
	}
}
