package app.controller;

import app.model.exceptions.InvalidInputException;
import app.model.kunde.AddressFields;
import app.model.kunde.Adresse;
import app.view.MainView;

public class CreateAdresseController extends MainController{

	/**
	 * @return Adresse from user input
	 */
	public static Adresse createAdresse() {
		Adresse adr = new Adresse();
		int input = 0;

		while (input < AddressFields.values().length) {
			try {
				switch (input) {
				case 0:
					adr.setAddressLine1(getUserInput(AddressFields.ADDRESSLINE1.toString()));
					input = 1;
				case 1:
					adr.setAddressLine2(getUserInput(AddressFields.ADDRESSLINE2.toString()));
					input = 2;
				case 2:
					adr.setZip(getUserInput(AddressFields.ZIP.toString()));
					input = 3;
				default:
					adr.setCity(getUserInput(AddressFields.CITY.toString()));
					input = 4;
				}
			} catch (InvalidInputException e) {
				MainView.retryPrompt(e.toString());
			}
		}
		return adr;
	}

}
