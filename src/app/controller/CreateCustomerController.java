package app.controller;

import app.model.exceptions.CustomerAlreadyExistingException;
import app.model.exceptions.InvalidInputException;
import app.model.kunde.Firmenkunde;
import app.model.kunde.FirmenkundeFields;
import app.model.kunde.Kunde;
import app.model.kunde.KundeFields;
import app.model.kunde.Privatkunde;
import app.model.kunde.PrivatkundeFields;
import app.testing.TestBankFactory;
import app.view.MainView;

class CreateCustomerController extends MainController {

	/**
	 * @param className Firmenkunde.class.getName() or Privatkunde.class.getName()
	 * @return
	 * @throws CustomerAlreadyExistingException
	 */
	public static Kunde newKunde(String className) throws CustomerAlreadyExistingException {
		Kunde k = null;

		if (className == Privatkunde.class.getName())
			k = createPrivatkunde();
		if (className == Firmenkunde.class.getName())// TODO else?
			k = createFirmenkunde();

		if (B.kundeAdd(k)) {
			MainView.showCustomerDetails(k);
			confirm();
		} else
			throw new CustomerAlreadyExistingException("This ain't a new customer");

		return k;
	}

	/**
	 * @return
	 */
	private static Firmenkunde createFirmenkunde() {
		Firmenkunde fk = (Firmenkunde) createKunde(new Firmenkunde());
		int input = 0;

		while (input < FirmenkundeFields.values().length) {
			try {
				switch (input) {
				case 0:
					fk.setCompany(getUserInput(FirmenkundeFields.COMPANY.toString()));
					input = 1;
				case 1:
				}
			} catch (InvalidInputException iie) {
				MainView.retryPrompt(iie.toString());
			}
		}
		fk.setContact(TestBankFactory.contactGenerator());//TODO this is the standard contact	
		return fk;
	}

	/**
	 * @return
	 */
	private static Privatkunde createPrivatkunde() {
		Privatkunde pk = (Privatkunde) createKunde(new Privatkunde());

		int input = 0;

		while (input < PrivatkundeFields.values().length) {
			try {
				switch (input) {
				case 0:
					pk.setFirstName(getUserInput(PrivatkundeFields.FIRSTNAME.toString()));
					input = 1;
				case 1:
					pk.setLastName(getUserInput(PrivatkundeFields.LASTNAME.toString()));
					input = 2;
				case 2:
					pk.setDob(CreateBirthdateController.createBirthdate());
					input = 3;
				case 3:
				}
			} catch (InvalidInputException iie) {
				MainView.retryPrompt(iie.getMessage());
			}
		}
		return pk;
	}

	/**
	 * @param kunde
	 * @return
	 */
	private static Kunde createKunde(Kunde kunde) {
		kunde.setAddress(CreateAdresseController.createAdresse());

		int inputCounter = 0;

		while (inputCounter < KundeFields.values().length) {
			try {
				switch (inputCounter) {
				case 0:
					kunde.setTelephone(getUserInput(KundeFields.TELEPHONE.toString()));
					inputCounter = 1;
				case 1:
					kunde.setEmail(getUserInput(KundeFields.EMAIL.toString()));
					inputCounter = 2;
				}
			} catch (InvalidInputException iie) {
				MainView.retryPrompt(iie.toString());
			}
		}

		return kunde;
	}
}