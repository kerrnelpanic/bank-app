package app.model.kunde;

import app.model.Ansprechpartner;
import app.model.exceptions.InvalidInputException;

/**
 * some useful information about this very, very complex class
 *
 */
public class Firmenkunde extends Kunde {
	private String company;
	private Ansprechpartner contact;

	/**
	 * @return
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company String of max length = FirmenkundeFields.COMPANY.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setCompany(String company) throws InvalidInputException {
		if (company.length() > FirmenkundeFields.COMPANY.getStrlen())
			throw new InvalidInputException("input \"" + company + "\" to long for " + FirmenkundeFields.COMPANY
					+ " allowed length: " + FirmenkundeFields.COMPANY.getStrlen());
		if (company.length() == 0)
			throw new InvalidInputException(
					"input \"" + company + "\" cannot be enpty for " + FirmenkundeFields.COMPANY.getStrlen());

		this.company = company;
	}

	/**
	 * @return
	 */
	public Ansprechpartner getContact() {
		return contact;
	}

	/**
	 * @param contact
	 */
	public void setContact(Ansprechpartner contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "\n___________________________________________\nFirmenkunde\nName:\t" + company + "\nAnsprechpartner:\t"
				+ contact.toString() 
				+ super.toString()
				+ "\n___________________________________________\n";
	}

	@Override
	public String getName() {
		return this.company;
	}

}
