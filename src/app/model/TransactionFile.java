package app.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

/**
 *
 *
 */
public class TransactionFile {

	private static final String[] TRANSACTION_HEADER = { "IBAN", "Zeitstempel", "Transaktionsart", "Beschreibungstext",
			"Betrag" };

	public static String[] getTransactionHeader() {
		String[] copy = new String[TRANSACTION_HEADER.length];
		System.arraycopy(TRANSACTION_HEADER, 0, copy, 0, TRANSACTION_HEADER.length);
		return copy;
	}

	public static void main(String[] args) {
//		for (int i = 0; i < readTransactions().length; i++) {
//			System.out.println(readTransactions()[i]);
//		}

//		flushTransactions(readTransactions());
//		LinkedList<String[]> transactionsAsList = readTransactions();
//		String[][] transactions = new String[transactionsAsList.size()][];
//		for (int i = 0; i < transactions.length; ++i) {
//			transactions[i] = transactionsAsList.get(i);
//		}
//
//		writeTransactions(transactions);
//		for (String[] s : readTransactions()) {
//			for (String str : s) {
//				System.out.print(str);
//			}
//			System.out.println();
//		}
//		System.out.println(readTransactions().get(0)[0]);
	}

	/**
	 * saves <b>oldTransactions</b> and recently committed transactions to a csv
	 * file
	 * 
	 * @param header       content of first line
	 * @param transactions already stored in a file
	 * @return true on success, false on failure
	 */
	public static boolean flushTransactions(LinkedList<String[]> transactions) {

		StringBuilder header = new StringBuilder();
		for (String s : TRANSACTION_HEADER) {
			header.append(s + ";");
		}
		header.deleteCharAt(header.length() - 1);

		LinkedList<String> lines = new LinkedList<String>();
		for (String[] transaction : transactions) {
			StringBuilder sb = new StringBuilder();
			for (String field : transaction) {
				sb.append(field + ";");
			}
			sb.deleteCharAt(sb.length() - 1);
			lines.add(sb.toString());
		}

		return writeTransactions(header.toString(), lines);
	}

	private static boolean writeTransactions(String header, LinkedList<String> transactions) {
		boolean result = false;
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter("transactions.txt"));
			pw.println(header);

			for (String s : transactions) {
				pw.println();
				pw.println(s);
			}
			result = true;

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			pw.close();
		}
		return result;
	}

	/**
	 * @return
	 */
	public static LinkedList<String[]> readTransactions() {

		LinkedList<String[]> transactions = new LinkedList<String[]>();
		BufferedReader br = null;
//TODO leave out first line, spare lines
		try {
			br = new BufferedReader(new FileReader("transactions.txt"));
			String line;
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] splitted = line.split(";");
				if (splitted.length > 0 && splitted[0].length() > 0)
					transactions.add(splitted);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return transactions;

//		String[] strArr = new String[strArrList.size()];
//		for (int i = 0; i < strArr.length; i++) {
//			strArr[i] = strArrList.get(i);
////			System.out.println(strArrList.get(i));
//		}

//		return strArr;
	}

}