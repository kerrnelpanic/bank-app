package app.model.exceptions;

public class CustomerAlreadyExistingException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerAlreadyExistingException(String message) {
		super(message);
	}
}
