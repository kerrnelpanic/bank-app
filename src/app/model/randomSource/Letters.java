package app.model.randomSource;

public enum Letters {
	a(true), b(false), c(false), d(false), e(true), f(false), g(false), h(false), i(true), j(false), k(false), l(false),
	m(false), n(false), o(true), p(false), q(false), r(false), s(false), t(false), u(true), v(false), w(false),
	x(false), y(false), z(false), ä(true), ö(true), ü(true), NotALetter(false);

	private boolean isVowel;
	public static final Letters[] VOWELS = Letters.vowels();
	public static final Letters[] CONSONANTS = Letters.consonants();

	Letters(boolean isVowel) {
		this.isVowel = isVowel;
	}

	public static Letters getLetter(int ordinal) {
		for (Letters l : Letters.values()) {
			if (l.ordinal() == ordinal)
				return l;
		}
		return NotALetter;
	}

	public boolean isVowel() {
		return isVowel;
	}

	private static Letters[] vowels() {
		Letters[] vowels = {};
		for (Letters l : Letters.values()) {
			if (l.isVowel) {
				Letters[] aux = new Letters[vowels.length + 1];
				for (int i = 0; i < vowels.length; i++) {
					aux[i] = vowels[i];
				}
				aux[vowels.length] = l;
				vowels = aux;
			}
		}
		return vowels;
	}

	private static Letters[] consonants() {
		Letters[] consonants = {};
		for (Letters l : values()) {
			if (!l.isVowel) {
				Letters[] aux = new Letters[consonants.length + 1];
				for (int i = 0; i < consonants.length; i++) {
					aux[i] = consonants[i];
				}
				aux[consonants.length] = l;
				consonants = aux;
			}
		}
		return consonants;
	}
}