package app.model;

import java.util.LinkedList;

import app.model.kunde.Adresse;
import app.model.kunde.Kunde;

/**
 * some useful information about this very, very complex class
 *
 */
public class Bank {
	private final String name;
	private final String bic;
	private final Adresse address;
	private LinkedList<Kunde> customers;

	/**
	 * @param name
	 * @param bic
	 * @param address
	 */
	public Bank(String name, String bic, Adresse address) {
		this.name = name;
		this.bic = bic;
		this.address = new Adresse(address);
		customers = new LinkedList<>();
	}

	/**
	 * @return name of bank
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return bic of bank
	 */
	public String getBic() {
		return bic;
	}

	/**
	 * @return address of bank
	 */
	public Adresse getAddress() {
		return new Adresse(this.address);
	}

	/**
	 * @param k
	 * @return true if <b>k</b> is added, false if <b>k</b> already exists in
	 *         <b>customers</b>
	 */
	public boolean kundeAdd(Kunde k) {
		if (!this.customers.contains(k) && k != null) {
			customers.add(k);
			return true;
		}

		return false;
	}

	/**
	 * @return a List of all known customers
	 */
	public LinkedList<Kunde> getCustomers() {
		return new LinkedList<>(customers);
	}

	@Override
	public String toString() {
		return "\n" + name + "\n" + bic + "\n" + address + "\n";
	}

}
