package app.controller;

import java.time.LocalDate;

import app.model.ValidateLocalDate;
import app.model.exceptions.InvalidInputException;
import app.view.MainView;

public class CreateBirthdateController {

	/**
	 * @return LocalDate from user input
	 */
	public static LocalDate createBirthdate() {
		LocalDate ld = null;
		while (ld == null) {
			try {
				String[] args = new String[3];
				args[0] = MainController.getUserInput("year");
				String rawMonth = MainController.getUserInput("month");
				if (rawMonth.length()>0)
					args[1] = rawMonth.substring(0, 1).toUpperCase() + rawMonth.substring(1).toLowerCase();
				args[2] = MainController.getUserInput("day");

				ld = ValidateLocalDate.validate(args);
			} catch (InvalidInputException ide) {
				MainView.retryPrompt(ide.getMessage());
				;
			}
		}
		return ld;
	}
}
