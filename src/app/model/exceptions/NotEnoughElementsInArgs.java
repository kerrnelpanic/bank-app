package app.model.exceptions;

public class NotEnoughElementsInArgs extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotEnoughElementsInArgs(String message) {
		super(message);

	}

}