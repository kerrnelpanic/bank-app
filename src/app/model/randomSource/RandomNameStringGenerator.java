package app.model.randomSource;

import java.util.Random;

public class RandomNameStringGenerator {
	public static String randomNameString() {
		return randomNameString(3, 5);
	}

	/**
	 * @param minLength a value between 1 to Integer.MAX_VALUE - 1;
	 * @param maxLength must be > minLength
	 * @return
	 */
	public static String randomNameString(int minLength, int maxLength) {

		StringBuilder sb = new StringBuilder();
		Letters l;
//		= Letters.v;
		if (new Random().nextInt() % 3 == 0)
			l = (getRandomConsonant());
		else
			l = (getRandomVowel());
		sb.append(l.toString().toUpperCase());

		for (int j = 0; j < new Random().nextInt(maxLength - minLength +1) + minLength -1; j++) {

			if (l.isVowel()) {
				if (new Random().nextInt() % 4 == 0)
					l = getRandomVowel();
				else
					l = getRandomConsonant();
			} else {
				if (new Random().nextInt() % 20 == 0)
					l = getRandomConsonant();
				else
					l = getRandomVowel();
			}
			sb.append(l);
		}
		return sb.toString();
	}

	private static Letters getRandomVowel() {
		int r = new Random().nextInt(Letters.VOWELS.length - 1);
		// reduce occurrence of umlauts
		if (r > 4)
			r = r - new Random().nextInt(5);
		return Letters.VOWELS[r];
	}

	private static Letters getRandomConsonant() {
		int r = new Random().nextInt(Letters.CONSONANTS.length - 1);
		return Letters.CONSONANTS[r];
	}
}