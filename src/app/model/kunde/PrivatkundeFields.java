package app.model.kunde;

public enum PrivatkundeFields {
	FIRSTNAME(10), LASTNAME(10);

	private int strlen;

	private PrivatkundeFields(int strlen) {
		this.strlen = strlen;
	}

	public int getStrlen() {
		return strlen;
	}

	public PrivatkundeFields getOrdinal(int ordinal) {
		for (PrivatkundeFields pf : PrivatkundeFields.values()) {
			if (pf.ordinal() == ordinal)
				return pf;
		}
		return null;
	}
}