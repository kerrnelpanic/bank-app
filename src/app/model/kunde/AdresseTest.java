package app.model.kunde;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class AdresseTest {

	@ParameterizedTest(name = "{0} ")
	@CsvSource({ 
			"usualValues:, Name, Zusatz, D-12345, Stadt" })
	void testConstructor(String testName, String line1, String line2, String zip, String city) {
		Adresse actual = new Adresse();
		
		assertEquals(line1, actual.getAddressLine1());
		assertEquals(line2, actual.getAddressLine2());
		assertEquals(zip, actual.getZip());
		assertEquals(city, actual.getCity());
	}

}
