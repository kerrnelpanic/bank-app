package app.model.kunde;

import java.util.ArrayList;

import app.model.exceptions.InvalidInputException;
import app.model.konto.Konto;

/**
 * some useful information about this very, very complex class
 *
 */
public abstract class Kunde implements Comparable<Kunde> {
	public static final String IDPREFIX = "KN-";
	private static int nextID = 1;

	private String customerID;
	private String telephone;
	private String email;
	private Adresse address;
	private ArrayList<Konto> accounts;

	/**
	 * no-args constructor 
	 */
	Kunde() {
		this.customerID = IDPREFIX + nextID++;
		this.address = new Adresse();
		this.accounts = new ArrayList<Konto>();
	}

	/**
	 * @return
	 */
	abstract public String getName();
	
	/**
	 * @return
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone String of max length = KUNDE_FIELDS.TELEPHONE.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setTelephone(String telephone) throws InvalidInputException {
		if (telephone.length() > KundeFields.TELEPHONE.getStrlen())
			throw new InvalidInputException("input \"" + telephone + "\" to long for " + KundeFields.TELEPHONE
					+ " allowed length: " + KundeFields.TELEPHONE.getStrlen());

		this.telephone = telephone;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email String of max length = FIRMENKUNDEN_FIELDS.EMAIL.getStrlen()
	 * @throws InvalidInputException
	 */
	public void setEmail(String email) throws InvalidInputException {
		if (email.length() > KundeFields.EMAIL.getStrlen())
			throw new InvalidInputException("input \"" + email + "\" to long for " + KundeFields.EMAIL
					+ " allowed length: " + KundeFields.EMAIL.getStrlen());

		this.email = email;
	}

	/**
	 * @return
	 */
	public Adresse getAddress() {
		return new Adresse(address);
	}

	/**
	 * @param address 4 Strings, ordered as addressLine1, addressLine2, zip, city
	 * @throws InvalidInputException
	 */
	public void setAddress(String... address) throws InvalidInputException {
		Adresse adrBuilder = new Adresse();
		adrBuilder.setAddressLine1(address[0]);
		adrBuilder.setAddressLine2(address[1]);
		adrBuilder.setZip(address[2]);
		adrBuilder.setCity(address[3]);
		this.address = adrBuilder;
	}

	/**
	 * @param address
	 */
	public void setAddress(Adresse address) {
		this.address = new Adresse(address);
	}

	/**
	 * @return
	 */
	public String getCustomerID() {
		return customerID;
	}

	/**
	 * @param k
	 * @return true if <b>k</b> could be added, false otherwise
	 */
	public boolean kontoAdd(Konto k) {
		if (accounts.size() < 11 && !this.accounts.contains(k)) {
			accounts.add(k);
			return true;
		}
		return false;
	}

	/**
	 * @return a list of all accounts belonging to this customer
	 */
	public ArrayList<Konto> getAccounts() {
		return new ArrayList<Konto>(accounts);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kunde other = (Kunde) obj;
		if (customerID == null) {
			if (other.customerID != null)
				return false;
		} else if (!customerID.equals(other.customerID))
			return false;
		return true;
	}
	

	@Override
	public int compareTo(Kunde o) {//TODO einfach die strings vergleichen?
		Integer o1id = Integer.parseInt(this.customerID.substring(IDPREFIX.length()));
		Integer o2id = Integer.parseInt(o.customerID.substring(IDPREFIX.length()));
		return o1id.compareTo(o2id);
	}
	

	@Override
	public String toString() {
		return "\nKdNr.:\t" + customerID + "\nTel.:\t" + telephone + "\nE-Mail:\t" + email + "\nAdresse:" + address
				+ "\n" + accounts.toString();
	}

}
